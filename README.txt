Konstantinos Theodorakos 

Technical Test - EMBL-EBI Position EBI_00816

a) The compiled software is located in: "Article XML Parser\Article XML Parser\bin\Release\Article XML Parser.exe" and the test xml file: "Article XML Parser\Article XML Parser\ebiMedlineArticles.xml".

I used:
	1. Object oriented C# and Windows Forms UI.
	2. File dialog for reading xml input files from disk.
	3. XMLDocument, XMLNodeList and XMLNode objects to load the text into memory and then browse the xml format sub-nodes.
	4. Dictionaries, Hashsets and List object collections. Also a ConcurrentDictionary collection for parallel thread access.
	5. Parallelized the search for common articles of author combinations (using the C# Parallel.Foreach concurrent thread method & lambda).
	6. Output to a data grid.

b) I would perform several test types:
	1. Unit testing: First, I would split the main execution method into several small methods and try/catch exception handling. I would then use the AAA 
	pattern (Arrange, Act, Assert) to test every method for ONE functionality only. Each test would check:
		i. Correct output using different valid (small) input combinations (i.e correct input xml objects, read not by disk but by a local variable - string inside the unit test case for speed).
		ii. Correct exception handling using invalid output combinations (i.e incorrect input xml objects, read not by disk but by a local variable - string inside the unit test case for speed).
	2. Integration testing: I would have several .xml files stored on the physical disk and perform the (i) and (ii) checks.
	3. Load/Performance testing: I would have several very large .xml stored on the physical disk. These would be checked against the speed of execution and correctness of the output results
	of the parallelised execution.

c) I would keep the UI in C# (or other high level language like Java or python) and the port the "slow code" into C++ using a library like Thread Building Blocks (https://www.threadingbuildingblocks.org).
First, the data collections to be accessed by parallelism/distributed nodes must be thread safe (i.e https://www.threadingbuildingblocks.org/tutorial-intel-tbb-concurrent-containers).

I have already parallelized the second "match making" for loop, using data parallelism. This was relatively easy because even though threads may store results to the same item in the returning collection I used
a C# thread-safe concurrent collection (so no read-write thread conflicts will occur). This parallel for loop could be scaled to many computing nodes in a cluster : first splitting the "Articles" work
first by node and then by every available thread worker. Ofcourse
each worker should store results to a concurrent collection (I used ConcurrentDictionary). Another solution to split the work to different computing nodes would be to use the "MapReduce" pattern.

The harder part to parallelize is the first loop, the xml reading loop, due to its tree structure. The solution would be to create a task dependency graph (i.e using a flow graph with the C++ template
library https://www.threadingbuildingblocks.org/tutorial-intel-tbb-flow-graph). I would then split the XML branches with "breadth first search and depth first stealing" to assign work loads to different workers.
Breadth first to use as many concurrent workers as possible. Depth first stealing would be applied by the workers that have finished all their jobs (so they can "steal" work from other worker nodes that don't have dependencies).

An issue in general, would be to ensure that there are no "reads and writes" at the same time on the same object of a collection. This can be avoided by either using mutexes or the concurrent containers that
already perform this checks and manage the parallel thread read/writes.

Last, to take the scaling even further, I would do:
- Use GPGPU (OpenCL or Cuda) frameworks (so thousands of "worker cores" for each pc node).
- Use MPI interface (very fast communication between every node via the Message Passing Interface).