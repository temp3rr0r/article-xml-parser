﻿namespace Article_XML_Parser
{
    partial class ArticleXmlParser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btwBrowseXml = new System.Windows.Forms.Button();
            this.authorToAuthorDataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.authorToAuthorDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // btwBrowseXml
            // 
            this.btwBrowseXml.Location = new System.Drawing.Point(537, 332);
            this.btwBrowseXml.Name = "btwBrowseXml";
            this.btwBrowseXml.Size = new System.Drawing.Size(75, 23);
            this.btwBrowseXml.TabIndex = 0;
            this.btwBrowseXml.Text = "Read XML";
            this.btwBrowseXml.UseVisualStyleBackColor = true;
            this.btwBrowseXml.Click += new System.EventHandler(this.btwBrowseXml_Click);
            // 
            // authorToAuthorDataGridView
            // 
            this.authorToAuthorDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.authorToAuthorDataGridView.Location = new System.Drawing.Point(12, 12);
            this.authorToAuthorDataGridView.Name = "authorToAuthorDataGridView";
            this.authorToAuthorDataGridView.Size = new System.Drawing.Size(606, 314);
            this.authorToAuthorDataGridView.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 361);
            this.Controls.Add(this.authorToAuthorDataGridView);
            this.Controls.Add(this.btwBrowseXml);
            this.Name = "Form1";
            this.Text = "Article XML Parser";
            ((System.ComponentModel.ISupportInitialize)(this.authorToAuthorDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btwBrowseXml;
        private System.Windows.Forms.DataGridView authorToAuthorDataGridView;
    }
}

