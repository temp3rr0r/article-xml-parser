﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Article_XML_Parser
{
    public partial class ArticleXmlParser : Form
    {
        public ArticleXmlParser()
        {
            InitializeComponent();
        }

        private void btwBrowseXml_Click(object sender, EventArgs e)
        {
            //
            // Open the file
            //

            OpenFileDialog xmlOpenFileDialog = new OpenFileDialog
            {
                Filter = @"xml files (*.xml)|*.xml",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            XmlDocument doc = new XmlDocument();
            if (xmlOpenFileDialog.ShowDialog() == DialogResult.OK)
                doc.Load(xmlOpenFileDialog.FileName);

            if (doc.DocumentElement != null)
            {
                //
                // Read article information from xml document
                //

                XmlNodeList articleNodes = doc.DocumentElement.SelectNodes("/MedlineCitationSet/Article");
                Dictionary<string, List<string>> articleToAuthorsDictionary = new Dictionary<string, List<string>>();
                HashSet<string> distinctAuthorsHashSet = new HashSet<string>();
                if (articleNodes != null)
                {
                    foreach (XmlNode article in articleNodes)
                    {
                        if (article != null)
                        {
                            string currentArticleTitle = article.SelectSingleNode("ArticleTitle")?.InnerText;

                            XmlNodeList authorNodeList = article.SelectNodes("AuthorList");
                            // For all the authors, add them to the dictionary
                            List<string> authorsList = new List<string>();
                            if (authorNodeList != null)
                            {
                                foreach (XmlNode authorList in authorNodeList)
                                {
                                    foreach (XmlNode author in authorList)
                                    {
                                        string authorFullName = $"{author.SelectSingleNode("ForeName")?.InnerText}"
                                                                +
                                                                $", {author.SelectSingleNode("LastName")?.InnerText}";
                                        distinctAuthorsHashSet.Add(authorFullName);
                                        authorsList.Add(authorFullName);
                                    }
                                }
                            }

                            if (currentArticleTitle != null)
                                articleToAuthorsDictionary[currentArticleTitle] = authorsList;
                        }
                    }

                    //
                    // Find common articles
                    //

                    ConcurrentDictionary<Tuple<string, string>, int> authorToAuthorDictionary =
                        new ConcurrentDictionary<Tuple<string, string>, int>(); // Concurrent dictionary to avoid thread read/write conflicts
                    Parallel.ForEach(articleToAuthorsDictionary.Values, authorsList => // Use parallelization Lambda
                    {
                        if (authorsList != null)
                        {
                            foreach (string firstAuthor in authorsList)
                            {
                                foreach (string secondAuthor in authorsList)
                                {
                                    Tuple<string, string> firstCombination = new Tuple<string, string>(firstAuthor, secondAuthor);
                                    if (!authorToAuthorDictionary.ContainsKey(firstCombination))
                                        authorToAuthorDictionary.TryAdd(firstCombination, 1); // Thread-safe write
                                    else
                                        authorToAuthorDictionary[firstCombination]++;

                                    Tuple<string, string> secondCombination = new Tuple<string, string>(secondAuthor,
                                        firstAuthor);
                                    if (!authorToAuthorDictionary.ContainsKey(secondCombination))
                                        authorToAuthorDictionary.TryAdd(secondCombination, 1); // Thread-safe write
                                    else
                                        authorToAuthorDictionary[secondCombination]++;
                                }
                            }
                        }
                    });

                    // Display the matrix

                    List<string[]> dataGridRows = new List<string[]>();
                    authorToAuthorDataGridView.Columns.Add("", "");
                    foreach (string firstAuthor in distinctAuthorsHashSet)
                    {
                        authorToAuthorDataGridView.Columns.Add(firstAuthor, firstAuthor);
                        List<string> currentDatGridRow = new List<string> {firstAuthor};
                        foreach (string secondAuthor in distinctAuthorsHashSet)
                        {
                            int combinationCount = 0;

                            Tuple<string, string> currentKey = new Tuple<string, string>(firstAuthor, secondAuthor);
                            if (authorToAuthorDictionary.ContainsKey(currentKey))
                                combinationCount = authorToAuthorDictionary[currentKey];
                            currentDatGridRow.Add(combinationCount.ToString());
                        }
                        dataGridRows.Add(currentDatGridRow.ToArray());
                    }
                    foreach (object[] dataGridRow in dataGridRows)
                        authorToAuthorDataGridView.Rows.Add(dataGridRow);
                }
            }
        }
    }
}
